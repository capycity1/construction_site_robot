#include <Servo.h>

Servo myservo;

// Pin, an dem sich der Servomotor befindet
const int PIN = A0;

// Winkel, der vom Servomotor angesteuert werden soll
// Winkel für Servomotor 0: 90
// Winkel für Servomotor 1: 60
// Winkel für Servomotor 2: 130
// Winkel für Servomotor 3: 0
const int ANGLE = 90;

void setup() {
  // Verbindung mit Servomotor an PIN herstellen
  myservo.attach(PIN);

    // Servomotor an in ANGLE festgelegten Winkel fahren
  myservo.write(ANGLE);
}

void loop() {

}