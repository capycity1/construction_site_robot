#define PART1
#define PART2
#define PART3

#include <Servo.h>

#ifdef PART1
// Platzhalter für Konfiguration der Servomotoren
#ifdef PART3
// Variablen der Servomotoren
Servo servo0;
Servo servo1;
Servo servo2;
Servo servo3;

// Konstenten mit Minimumwerten der Servomotorwinkel
const int MIN_ANGLE_SERVO_0 = 0;
const int MIN_ANGLE_SERVO_1 = 0;
const int MIN_ANGLE_SERVO_2 = 35;
const int MIN_ANGLE_SERVO_3 = 0;

// Konstanten mit Maximumwerten der Servomotorwinkel
const int MAX_ANGLE_SERVO_0 = 180;
const int MAX_ANGLE_SERVO_1 = 180;
const int MAX_ANGLE_SERVO_2 = 180;
const int MAX_ANGLE_SERVO_3 = 110;

// Konstanten mit Anschlüssen der Servomotoren
const int PIN_SERVO_0 = A1;
const int PIN_SERVO_1 = A0;
const int PIN_SERVO_2 = 8;
const int PIN_SERVO_3 = 9;

// Variablen der Winkel der Servomotoren
int angleServo0 = 90;
int angleServo1 = 60;
int angleServo2 = 60;
int angleServo3 = 0;

#endif

void setup() {
  // Initialisere serielle Kommunikation für Ausgaben
  Serial.begin(9600);
  
  // Platzhalter für Servomotoren in Setup

  #ifdef PART3
  // Weise Pins den entsprechenden Servomotoren zu
  servo0.attach(PIN_SERVO_0);
  servo1.attach(PIN_SERVO_1);
  servo2.attach(PIN_SERVO_2);
  servo3.attach(PIN_SERVO_3);

  // Fahre Servomotoren an aktuelle Winkel als Startposition
  servo0.write(angleServo0);
  servo1.write(angleServo1);
  servo2.write(angleServo2);
  servo3.write(angleServo3);
  #endif
}

void loop() {  
  // Lese die Werte der Analogsticks aus
  // uns speichere diese in einzelnen Variablen
  int stickXL = analogRead(A3);
  int stickYL = analogRead(A4);
  int stickXR = analogRead(A2);
  int stickYR = analogRead(A5);

  // Gebe Werte von Analogsticks aus
  // Serial.print("stickXL = " + String(stickXL) + "; ");
  // Serial.print("stickYL = " + String(stickYL) + "; ");
  // Serial.print("stickXR = " + String(stickXR) + "; ");
  // Serial.print("stickYR = " + String(stickYR) + ";\n\r");

  // Platzhalter für Festlegung der Bewegungen
  #ifdef PART2
  // Bilde Werte der Analogsticks (0 bis 1023) auf
  // Geschwindigkeiteswerte (-3 bis 3) der Bewegungs einzelner Servomotoren ab
  // und speichere den neuen Wert in der entsprechenden Variable an
  stickXL = map(stickXL, 0, 1023, -3, 3); 
  stickYL = map(stickYL, 0, 1023, -3, 3); 
  stickXR = map(stickXR, 0, 1023, -3, 3); 
  stickYR = map(stickYR, 0, 1023, -3, 3); 

  // Gebe Werte der Bewegungsgeschwindigkeit aus
  // Serial.print("stickXL = " + String(stickXL) + "; ");
  // Serial.print("stickYL = " + String(stickYL) + "; ");
  // Serial.print("stickXR = " + String(stickXR) + "; ");
  // Serial.print("stickYR = " + String(stickYR) + ";\n\r");
  #endif

  // Platzhalter Bewegung der Servomotoren
  #ifdef PART3
  // Ändere Winkel der Servomotoren, abhängig von der Geschwindigkeit
  angleServo0 = angleServo0 + stickXL;
  angleServo1 = angleServo1 + stickYL;
  angleServo2 = angleServo2 + stickYR;
  angleServo3 = angleServo3 + stickXR;

  // Prüfe ob Winkel außerhalb der Grenzwerte sind.
  // Setze diese ggf. auf entsprechenden Grenzwert zurück.
  if(angleServo0 < MIN_ANGLE_SERVO_0) {angleServo0 = MIN_ANGLE_SERVO_0;}
  if(angleServo0 > MAX_ANGLE_SERVO_0) {angleServo0 = MAX_ANGLE_SERVO_0;}

  if(angleServo1 < MIN_ANGLE_SERVO_1) {angleServo1 = MIN_ANGLE_SERVO_1;}
  if(angleServo1 > MAX_ANGLE_SERVO_1) {angleServo1 = MAX_ANGLE_SERVO_1;}

  if(angleServo2 < MIN_ANGLE_SERVO_2) {angleServo2 = MIN_ANGLE_SERVO_2;}
  if(angleServo2 > MAX_ANGLE_SERVO_2) {angleServo2 = MAX_ANGLE_SERVO_2;}

  if(angleServo3 < MIN_ANGLE_SERVO_3) {angleServo3 = MIN_ANGLE_SERVO_3;}
  if(angleServo3 > MAX_ANGLE_SERVO_3) {angleServo3 = MAX_ANGLE_SERVO_3;}

  servo0.write(angleServo0);
  servo1.write(angleServo1);
  servo2.write(angleServo2);
  servo3.write(angleServo3);
  #endif

  delay(7); // Warte sieben Millisekunden
}

#endif