---
titlepage: true
title: "Capycity Lagerroboter"
author: [Markus Hülß]
subject: "Arduino Workshop - Roboterarm"
keywords: [Arduino, Roboterarm, Workshops]
lang: "de-DE"
toc: true
toc-title: "Inhaltsverzeichnis"
toc-own-page: true
colorlinks: true
listings-no-page-break: true
header-includes:
- |
  ```{=latex}
  \usepackage{awesomebox}

  % \usepackage[export]{adjustbox}
  % \let\includegraphicsbak\includegraphics
  % \renewcommand*{\includegraphics}[2][]{\includegraphicsbak[frame,#1]{#2}}

  \usepackage{enumitem}

  % TODO: fix wrong language bug
  \usepackage{cleveref}

  \usepackage{amsthm}
  \newtheoremstyle{colon}%
  {3pt}
  {0pt}
  {\itshape}%bodyfont
  {}%indent
  {\bfseries}%headfont
  {:}%head punctuation
  { }%space after head
  {}

  \theoremstyle{colon}
  \newtheorem{step}{Schritt}
  \crefname{step}{Schritt}{Schritte}
 	
  \graphicspath{{./}{./doc/}{./doc/images}}

  ```
pandoc-latex-environment:
  noteblock: [note]
  tipblock: [tip]
  warningblock: [warning]
  cautionblock: [caution]
  importantblock: [important]
...

<style>
.float {
  float: right;
  width: 50%;
}
</style>

# Capycity Lagerroboter

## Prolog

Zur Erzeugung ausreichend erneuerbarer Energie wurde von einer Gruppe Capybaras beschlossen, eine Stadt namens Capycity aufzubauen, welche ihren Fokus auf die Erzeugung solcher Energie legt. Nachdem mithilfe einer selbst entwickelten Planungssoftware ein entsprechendes Konzept erarbeitet werden konnte, soll nun der Bau beginnen.
Als erstes soll dabei ein Windkraftwerk aufgebaut werden. Doch schon früh wurde erkannt, dass sie dieses nicht so einfach mit ihren Pfoten aufbauen können. Die erste Frage die sich stellt ist: "Wie sollen die benötigten Teile zur Baustelle kommen?". Sie müssten zunächst in einen Container geladen werden, um diese daraufhin zur Baustelle zu fahren. Doch ohne weitere Hilfsmittel wird das nicht gehen. Bei einer Versammlung, um gemeinsam nach Lösungen zu suchen, meldet sich Robotik-Expertin Lucca zu Wort. "Ich habe bei mir in der Werkstatt einen Roboterarm, zumindest so halb, aus Platzgründen habe ich ihn ein wenig zerlegt. Ich könnte die einzelnen Teile holen und den Arm vor Ort zusammenbauen." Die anderen Capybaras schauen sich gegenseitig kurz an und nicken dann Lucca zu. Daraufhin macht sie sich gleich auf den Weg in die Werkstatt.

## Kapitel 1 - Der Zusammenbau

### Die Einzelteile

Beim Erreichen ihrer Werkstatt sieht Lucca in der Ecke ein paar Kisten mit der Beschriftung "Roboterarm, wenn die eigenen Arme mal wieder zu schwach sind". Sie schnappt sich alle Kisten und schaut in jede, um zu prüfen, ob alle benötigten Teile da sind. Sie erblickt dabei folgende Teile:

 - [Bild mit Basis]
 - [Bild mit Arduino, Servomotoren, Jumper-Kabeln und Netzteil]
 - [Bild mit Werkzeug, Schrauben, einzelnen Teiles des Arms]

::: note 
**Prüfe, ob alle Teile vorhanden sind.**
:::

Nachdem sie festgestellt hat, dass alle Teile vorhanden sind, packt sie diese auf ihren Pickup und fährt damit zur Lagerhalle, in der sich die Einzelteile des Windkraftwerks befinden.
Bei der Lagerhalle angekommen, bringt Lucca alle Teile des Roboterarms in die Nähe der Windkraftwerkteile.
Sie legt diese zunächst wie folgt geordnet vor sich auf:

[Bild mit geordneten Teilen]

::: note 
**Lege dir die Teile wie auf dem Bild zurecht.**
:::

Beim Betrachten der Servomotoren fällt ihr ein, dass diese zuvor richtig eingestellt werden müssen. Sie wird daher wohl mit der grundsätzlichen Elektronik anfangen müssen, bevor sie den Arm zusammenbauen kann.

### Die Elektronik

Um die Servomotoren einzustellen, muss sie jeden einzelnen mithilfe des Mikrocontrollers auf einen bestimmten Startwinkel bringen.
Dafür muss sie zunächst die passende Software zum Programmieren des Mikrocontrollers installieren.

---

::: note
Installiere unter der [Arduino-Webseite](https://www.arduino.cc/en/software) ([https://www.arduino.cc/en/software](https://www.arduino.cc/en/software)) die **Arduino IDE**.
Der Einfachheit halber ist es unter Windows oder Linux zu empfehlen die ZIP-Datei zu verwenden und diese direkt zu entpacken
:::
![Download der Arduino-IDE für unterschiedliche Betriebssysteme](./images/arduino_download.png)

---

Nach der Installation des Software, startet sie diese und erstellt einen neuen Sketch (so werden Programme unter Arduino bezeichnet), um zunächst ein leeres Programm vor sich zu haben (\Cref{fig:new_sketch}).

![Erstellung eines neues Sketchs unter Windows \label{fig:new_sketch}](./images/new_sketch.png){width=90%}

Daraufhin fängt sie gleich an den notwendigen Code zu schreiben, mit dem sie die Servomotoren nach und nach einstellen kann (\Cref{lst:codeAdjustmentServo}).

```{caption="Quellcode zum Einstellen des Servomotors" label=lst:codeAdjustmentServo .cpp}
#include <Servo.h>

Servo myservo;

// Konstante mit Pin, an dem sich der Servomotor befindet
const int PIN = A0;

// Konstante mit Winkel, der vom Servomotor angesteuert werden soll
const int ANGLE = 90;

void setup() {
  // Verbindung mit Servomotor an PIN herstellen
  myservo.attach(PIN);

  // Servomotor an in ANGLE festgelegtem Winkel fahren
  myservo.write(ANGLE);
}

void loop() {}
```

Das Programm steht soweit, nun können die einzelnen Motoren eingestellt werden. Als Lucca den ersten Motor anstecken möchte bemerkt sie, dass das Kontrollboard noch gar nicht mit dem Mikrocontroller zusammengesteckt wurde. Also macht sie das noch schnell, um endlich das Programm aufspielen zu können.
Dazu müssen zunächst noch das Kontrollboard und der Arduino-Mikrocontroller zusammengesteckt werden.

:::note
**Stecke das Kontrollboard auf den Mikrocontroller, wie in [Abbildung zu sehen]**
:::

[Bild: Zusammenstecken von Kontrollboard und Arduino]

Danach kann sie endlich den ersten Servomotor an den **Port A0** anschließen sowie den Mikrocontroller noch schnell mit dem **USB-Kabel an den Laptop stecken**, um das Programm mithilfe des **Hochladen-Buttons** aufzuspielen

:::note
**Stecke den ersten Servomotor (Als S0 gekennzeichnet) an den Port A0 (\Cref{fig:connect_servo_to_mcu}) und schließe den Mikrocontroller mithilfe des USB-Kabels an den Laptop. Achte darauf, dass das braune Kabel auf der Seite ist, die mit "G" gekennzeichnet ist (sichtbar am unteren Bereich der Anschlüsse) und das rote Kabel auf der Seite mit "V". Zusätzlich muss vor dem Hochladen einmal geprüft werden, ob der richtige Mikrocontroller und Port, an dem sich dieser befindet, eingestellt wurde (\Cref{fig:select_board} und \Cref{fig:select_port}). Lade danach das Programm hoch (\Cref{fig:upload_program})**
:::

![Anschluss des Servomotors an Mikrocontroller. Quelle: \ref{keyestudioManual} \label{fig:connect_servo_to_mcu}](./images/servo_arduino_connection_medium.png){width=40%}

![Einstellen des Mikrocontrollers als Arduino UNO \label{fig:select_board}](./images/select_board.png)

![Einstellen des Ports, an dem sich der Mikrocontroller befindet (Name kann sich unterscheiden) \label{fig:select_port}](./images/select_port.png){width=75%}

![Hochladen des Programms \label{fig:upload_program}](images/upload.png){width=50%}

Nach dem Hochladen des Programs bewegt sich der Servomotor mit einem leisen Surren an die zuvor festgelegte Position.
Lucca freut sich, dass trotz der langen Lagerung alles noch gut funktioniert und macht gleich mit den anderen Motoren weiter.

:::note
**Stelle nun die Servomotoren S1, S2 und S3 ein. Stecke dafür immer den zuvor genutzten ab und schließe den nächsten einzustellenden Motor an den Port A0. Ändere die Konstante ANGLE so ab, dass der entsprechende Servomotor den richtigen Winkel ansteuert und lade das Programm für jeden Servomotor neu hoch (\Cref{lst:codeServoAngles}).**
:::

``` {caption="Konstante zur Einstellung des Winkels mit Auflistung benötigter Winkel für jeden Servomotor" label=lst:codeServoAngles .cpp}
// Winkel für Servomotor 0 (S0): 90
// Winkel für Servomotor 1 (S1): 60
// Winkel für Servomotor 2 (S2): 130
// Winkel für Servomotor 3 (S3): 0
const int ANGLE = 90;
```

### Der Zusammenbau des Arms

Nachdem die Elektronik soweit aufgebaut und eingestellt ist, kann Lucca nun endlich mit dem Roboterarm anfangen.
Doch sie stellt fest, dass es viele verschiedene Teile sind. Aber irgendwo gab es doch eine Anleitung. Lucca kramt ein wenig in den Karton herum, bis sie erfolgreich und mit einem Lächeln im Gesicht die zugehörige Anleitung in die Luft hält. Nun steht dem Zusammenbau nichts mehr im Wege.

:::note
**Baue die einzelnen Teile mithilfe der folgenden Anleitung zusammen.**
:::

<!-- Step 1 TODO: Correct space between step number and necessary parts-->
\begin{step}\label{step1}\end{step}
\textbf{Folgende Teile werden benötigt:}
\begin{itemize}
 \item[1x] Große Acryl-Platte
 \item[4x] M3x6mm-Rundkopfschrauben
 \item[4x] M3x10mm-Abstandshalter
\end{itemize}
Stecke die **M3x6mm-Rundkopfschrauben** durch die markierten Löcher in den unteren Ecken (\Cref{fig:build_base}) und befestige die **M3x10mm-Abstandshalter** auf der anderen Seite an den Schrauben. Achte, auf die **Orientierung der Acryl-Platte (siehe rote viereckige Markierungen)**

![Zusammenbau der Basis. Quelle: \ref{keyestudioManual} \label{fig:build_base}](./images/robot_arm_installation_01.png){width=75%}

<!-- Step 2 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_02.png}
\captionof{figure}{Montage des Batteriehalters an große Acryl-Platte. \\ Quelle: \ref{keyestudioManual}}\label{fig:build_battery_holder}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step2}\end{step}
\textbf{Folgende Teile werden benötigt:}
\begin{itemize}
 \item[1x] Basis aus \Cref{step1}
 \item[2x] M3x10mm-Flachkopfschraube
 \item[2x] M3-Schraubenmutter
 \item[1x] Batteriehalter
\end{itemize}
Stecke die \textbf{M3x10mm Flachkopfschrauben} durch die Löcher des \textbf{Batteriehalters} und stecke diesen dann durch dur Löcher am lochlosen Ende der Basis (\Cref{fig:build_battery_holder}). Befestige den \textbf{Batteriehalter} von unten mithilfe der \textbf{M3-Schraubenmuttern}.
\end{minipage}

<!-- Step 3 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_03.png}
\captionof{figure}{Montage der langen Abstandshalter an Basis. \\ Quelle: \ref{keyestudioManual}}\label{fig:build_big_spacers_base}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step3}\end{step}
\textbf{Folgende Teile werden benötigt:}
\begin{itemize}
 \item[1x] Basis aus \Cref{step1}
 \item[4x] M3x6mm-Rundkopfschraube
 \item[4x] M3x45mm-Abstandshalter
\end{itemize}
Befestige die \textbf{M3x45mm-Abstandshalter} mithilfe der \textbf{M3x6mm-Rundkopfschraube} an den markierten Löchern \Cref{fig:build_big_spacers_base}.
\end{minipage}

<!-- Step 4 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_04.png}
\captionof{figure}{Montage des Servomotors S0 an kleine Acryl-Platte. \\ Quelle: \ref{keyestudioManual}}\label{fig:build_servo0}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step4}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Kleine Acryl-Platte
 \item[1x] Servomotor S0
 \item[2x] M2x8mm-Schraube
 \item[2x] M2-Schraubenmutter
\end{itemize}
Stecke die \textbf{M2x8mm-Schrauben} durch die markierten Löcher, neben dem rechteckigen Loch (\Cref{fig:build_servo0}). Stecke dann von unten den Servomotor S0 über die Schrauben. Schraube zuletzt die \textbf{M2-Schraubenmuttern} über die Schrauben, um den Servomotor zu befestigen.
\end{minipage}

<!-- Step 5 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_05.png}
\captionof{figure}{Montage der kleinen Acryl-Platte an große Acryl-Platte. \\ Quelle: \ref{keyestudioManual}}\label{fig:mount_servo0}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step5}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Kleine Acryl-Platte mit Servomotor S0 aus \Cref{step4}
 \item[2x] M3x6mm-Rundkopfschraube
\end{itemize}
Lege die \textbf{kleine Acryl-Platte mit Servomotor S0 aus \Cref{step4}} auf die langen Abstandshalter der Basis und befestige diese mithilfe der \textbf{M3x6mm-Rundkopfschrauben} an dem markierten Löchern (\Cref{fig:mount_servo0}).
\end{minipage}

<!-- Step 6 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_06.png}
\captionof{figure}{Montage des Servohorns an U-Teil des Arms. \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_horn_servo0}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step6}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] U-Teil des Arms
 \item[1x] Servohorn (kreuzförmig, liegt Servomotor bei)
 \item[4x] 1.2x4mm Blechschraube (liegt Servomotor bei)
\end{itemize}
Stecke das \textbf{Servohorn} durch die Öffnung zwischen den Markierungen (\Cref{fig:mount_horn_servo0}). Befestige dieses danach von unten, mithilfe der \textbf{1.2x4mm Blechschrauben}, an den markierten Öffnungen.
\end{minipage}

<!-- Step 7 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_07.png}
\captionof{figure}{Montage des Servomotors S1 an U-Teil des Arms. \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_servo1}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step7}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] U-Teil des Arms (aus \Cref{step6})
 \item[1x] Servomotor S1
 \item[2x] M2x8mm-Rundkopfschraube
 \item[2x] M2-Schraubenmutter
\end{itemize}
Stecke den \textbf{Servomotor S1} durch die linke Öffnung des \textbf{U-Teils}. Befestige diesen dann mithilfe der \textbf{M2x8mm-Rundkopfschrauben} und \textbf{M2-Schraubenmutter} an den markierten Löchern (\Cref{fig:mount_servo1})
\end{minipage}

<!-- Step 8 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_08.png}
\captionof{figure}{Montage des Servohorns an 45mm Metall(Acryl)-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_horn_to_45mm_metal}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step8}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Servohorn (linienförmig-einfach, liegt Servomotor bei)
 \item[2x] 1.2x4mm Blechschraube (liegt Servomotor bei)
 \item[1x] 45mm Metall(Acryl)-Teil
\end{itemize}
Stecke das \textbf{Servohorn} durch die markierte Öffnung des \textbf{45mm Metall(Acryl)-Teils} und befestige dieses an den markierten Löchern (\Cref{fig:mount_horn_to_45mm_metal})
\end{minipage}

<!-- Step 9 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_09.png}
\captionof{figure}{Montage des Servohorns an 45mm Metall(Acryl)-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_45mm_metal_to_86mm_metal}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step9}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] 86mm Metall(Acryl)-Teil
 \item[1x] M3x8mm-Rundkopfschraube
 \item[1x] Selbstsichernde M3-Schraubenmutter
\end{itemize}
Stecke die \textbf{M3x8mm-Rundkopfschraube} durch das markierte Loch am \textbf{45mm Metall(Acryl)-Teil aus \Cref{step8}} (\Cref{fig:mount_45mm_metal_to_86mm_metal}). Die Schraube muss dabei auf der gleichen Seite wie das \textbf{Servohorn} sein. Stecke danach von der anderen Seite das \textbf{86mm Metall(Acryl)-Teil} darüber und befestige dieses mit der \textbf{selbstsicherndem M3-Schraubenmutter}.
\end{minipage}

<!-- Step 10 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_10.png}
\captionof{figure}{Montage der Metall(Acryl)-Teile an Servomotor S1 \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_metal_servo1}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step10}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] M2x5mm-Rundkopfschraube (liegt dem Servomotor bei)
\end{itemize}
Befestige das \textbf{Metall(Acryl)-Teil aus \Cref{step10}} wie in \Cref{fig:mount_metal_servo1} mithilfe der \textbf{M2x5mm-Rundkopfschraube}.
\end{minipage}

<!-- Step 11 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_11.png}
\captionof{figure}{Montage des L-Metall(Acryl)-Teils sowie Servohorns am Y-Metall(Acryl)-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_l_metal_on_y_metal}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step11}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] M3x8mm-Rundkopfschraube
 \item[1x] Selbstsichernde M3-Schraubenmutter
 \item[1x] L-Metall(Acryl)-Teil
 \item[1x] Y-Metall(Acryl)-Teil
 \item[2x] 1.2x4mm Blechschraube (liegt Servomotor bei)
 \item[1x] Servohorn (linienförmig-einfach, liegt Servomotor bei)
\end{itemize}
Befestige das \textbf{L-Metall(Acryl)-Teil} mithilfe der \textbf{M3x8mm-Rundkopfschraube} und der \textbf{selbstsichernden M3-Schraubenmutter} an der markierten Stelle des \textbf{Y-Metall(Acryl)-Teils}(\Cref{fig:mount_l_metal_on_y_metal}).\\
Befestige danach auf der anderen Seite des \textbf{Y-Metall(Acryl)-Teils} das \textbf{Servohorn} mithilfe der \textbf{1.2x4mm Blechschrauben} an der markierten Stelle (\Cref{fig:mount_l_metal_on_y_metal}).
\end{minipage}

<!-- Step 12 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_12.png}
\captionof{figure}{Platzierung des erweiterten Y-Metall(Acryl)-Teils an U-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:place_y_metal_on_u_part}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step12}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] U-Teil des Arms (aus \Cref{step10})
 \item[1x] Erweitertes Y-Metall(Acryl)-Teil (aus \Cref{step11})
\end{itemize}
Platziere das \textbf{U-Teil des Arms aus \Cref{step10}} auf das \textbf{erweiterte Y-Metall(Acryl)-Teil (aus \Cref{step11})} und halte dieses dort fest. Anders als in \Cref{fig:place_y_metal_on_u_part} zu sehen, wird der Servomotor erst im nächsten Schritt benötigt.
\end{minipage}

<!-- Step 13 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_13.png}
\captionof{figure}{Montage des Servomotors S2 an U-Teil und erweitertes Y-Metall(Acryl)-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_servo2}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step13}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Servomotor S2
 \item[2x] M2x8mm-Rundkopfschraube
 \item[2x] M2-Schraubenmutter
 \item[1x] M2x5mm-Rundkopfschraube (liegt dem Servomotor bei)
\end{itemize}
Stecke den \textbf{Servomotor S2} durch die rechte Öffnung des \textbf{U-Teils} und befestige ihn zunächst am \textbf{Servohorn} vom \textbf{erweiterten Y-Metall(Acryl)-Teil}. Befestige dann den \textbf{Servomotor S2} mithilfe der \textbf{M2x8mm-Rundkopfschrauben} und \textbf{M2-Schraubenmuttern} an den markierten Löchern des \textbf{U-Teils} (\Cref{fig:mount_servo2}). Schraube danach das die \textbf{M2x5mm-Rundkopfschraube} durch das \textbf{Servohorn}.
\end{minipage}

<!-- Step 14 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_14.png}
\captionof{figure}{Montage des erweiterten Y-Metall(Acryl)-Teils am U-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:place_y_metal_on_u_part}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step14}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] M3x8mm-Rundkopfschraube
 \item[1x] Selbstsichernde M3-Schraubenmutter
\end{itemize}
Befestige das \textbf{Y-Metall(Acryl)-Teil} mithilfe der \textbf{M3x8mm-Rundkopfschraube} und \textbf{selbstsichernde M3-Schraubenmutter} an der markierten Stelle des \textbf{U-Teils} (\Cref{fig:place_y_metal_on_u_part}).
\end{minipage}

<!-- Step 15 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_15.png}
\captionof{figure}{Montage des 86mm Metall-Teils am erweiterten U-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:place_86mm_metal_on_u_part}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step15}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Erweitertes U-Teil aus \Cref{step14}
 \item[1x] M3x8mm-Rundkopfschraube
 \item[1x] Selbstsichernde M3-Schraubenmutter
 \item[1x] 86mm Metall(Acryl)-Teil
\end{itemize}
Befestige mithilfe der \textbf{M3x8mm-Rundkopfschraube} und \textbf{selbstsichernden M3-Schraubenmutter} das \textbf{86mm Metall(Acryl)-Teil} an die Seite vom \textbf{erweiterten U-Teil aus \Cref{step14}}, an der sich auch der \textbf{Servomotor S2} befindet (\Cref{fig:place_86mm_metal_on_u_part}).
\end{minipage}

<!-- Step 16 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_16.png}
\captionof{figure}{Montage des 125mm Metall-Teils am erweiterten U-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:place_86mm_metal_on_u_part}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step16}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Erweitertes U-Teil aus \Cref{step15}
 \item[2x] M3x8mm-Rundkopfschraube
 \item[2x] Selbstsichernde M3-Schraubenmutter
 \item[1x] 125mm Metall(Acryl)-Teil
\end{itemize}
Befestige mithilfe der \textbf{M3x8mm-Rundkopfschrauben} und \textbf{selbstsichernden M3-Schraubenmuttern} das \textbf{125mm Metall(Acryl)-Teil} an den oberen Bereich des \textbf{erweiterten U-Teils (Y-Erweiterung)}. Es muss dabei an der Seite, an der sich der \textbf{Servomotor S1} befindet befestigt werden.
\end{minipage}

<!-- Step 17 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_17.png}
\captionof{figure}{Montage des 86mm/125mm/dreieckigen Metall-Teils am erweiterten U-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:place_triangle_metal_on_u_part}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step17}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Erweitertes U-Teil aus \Cref{step16}
 \item[2x] M3x8mm-Rundkopfschraube
 \item[1x] M3x10mm-Rundkopfschraube
 \item[3x] Selbstsichernde M3-Schraubenmutter
 \item[1x] dreieckiges Metall(Acryl)-Teil
 \item[1x] 86mm Metall(Acryl)-Teil
 \item[1x] 100mm Metall(Acryl)-Teil
\end{itemize}
Befestige mithilfe der \textbf{M3x8mm-Rundkopfschrauben}, \textbf{M3x10mm-Rundkopfschrauben} und der \textbf{selbstsichernden M3-Schraubenmuttern} das \textbf{dreieckige Metall(Acryl)-Teil} an der anderen Seite am oberen Bereich des \textbf{erweiterten U-Teils (Y-Erweiterung)}. Achte darauf, die passenden Teile und Schrauben an den entsprechenden Stellen zu verwenden sowie, dass das \textbf{dreieckige Metall(Acryl)-Teil} richtig herum ist (siehe \Cref{fig:place_triangle_metal_on_u_part}).
\end{minipage}

<!-- Step 18 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_18.png}
\captionof{figure}{Montage des T-Metall(Acryl)-Teils am kleinen U-Metall(Acryl)-Teil \\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_t_metal_on_small_u_metal}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step18}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Kleines U-Metall(Acryl)-Teil
 \item[1x] T-Metall(Acryl)-Teil
 \item[2x] M3x6mm-Rundkopfschraube
 \item[2x] Selbstsichernde M3-Schraubenmutter
\end{itemize}
Befestige mithilfe der \textbf{M3x6mm-Rundkopfschrauben} sowie \textbf{selbstsichernden M3-Schraubenmuttern} das \textbf{T-Metall(Acryl)-Teil} am \textbf{kleinen U-Metall(Acryl)-Teil}. Achte darauf, dass es so wie in \Cref{fig:mount_t_metal_on_small_u_metal} montiert wird.
\end{minipage}

<!-- Step 19 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_19.png}
\captionof{figure}{Montage des kleinen U-Metall(Acryl)-Teils an erweitertem U-Teil\\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_small_u_part_on_u_part}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step19}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Kleines U-Metall(Acryl)-Teil aus \Cref{step18}
 \item[1x] Erweitertes U-Teil aus \Cref{step17}
 \item[3x] M3x8mm-Rundkopfschraube
 \item[3x] Selbstsichernde M3-Schraubenmutter
\end{itemize}
Befestige mithilfe der \textbf{M3x8mm-Rundkopfschrauben} sowie \textbf{selbstsichernden M3-Schraubenmuttern} das \textbf{kleine U-Metall(Acryl)-Teil aus \Cref{step18}} am \textbf{erweiterten U-Teil aus \Cref{step17}}, wie in \Cref{fig:mount_small_u_part_on_u_part} ersichtlich. Das Armgerüst ist ab diesem Punkt soweit fertig.
\end{minipage}

<!-- Step 20 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_20.png}
\captionof{figure}{Montage Servomotor S3 an Armgerüst\\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_servo3}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step20}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Armgerüst aus \Cref{step19}
 \item[1x] Servomotor S3
 \item[2x] M2x12mm-Rundkopfschraube
 \item[2x] M2-Schraubenmutter
 \item[2x] Abstandshalter aus Kunststoff
\end{itemize}
Befestige den \textbf{Servomotor S3} mithilfe der \textbf{M2x12mm-Rundkopfschraube} und M2-Schraubenmutter an der rechteckigen Öffnung am oberen Bereich des \textbf{Armgerüsts aus \Cref{step19}}. Zwischen \textbf{Servomotor} und dem \textbf{Armgerüsts} müssen zuvor die \textbf{Abstandshalter aus Kunststoff} platziert werden (\Cref{fig:mount_servo3}).
\end{minipage}

<!-- Step 21 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_21.png}
\captionof{figure}{Montage der ersten Zangenhälfte an Servomotor S3\\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_claw1}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step21}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Armgerüst aus \Cref{step20}
 \item[1x] Zangenhälfte
 \item[1x] Servohorn (linienförmig-einfach, liegt Servomotor bei)
 \item[1x] M2x5mm-Rundkopfschraube (liegt dem Servomotor bei)
 \item[2x] 1.2x4mm Blechschraube (liegt Servomotor bei)
\end{itemize}
Befestige das \textbf{Servohorn} mithilfe der \textbf{1.2x4mm Blechschraube} an den markierten Stellen der \textbf{Zangenhälfte} (\Cref{fig:mount_claw1}). Stecke dann die \textbf{Zangenhälfte} wie in \Cref{fig:mount_claw1} auf den \textbf{Servomotor S3}. Befestige danach die \textbf{Zangenhälfte} zusätzlich mithilfe der \textbf{M2x5mm-Rundkopfschraube} am \textbf{Servomotor S3}.
\end{minipage}

<!-- Step 22 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_22.png}
\captionof{figure}{Verschraubung der Schraube für zweite Zangenhälfte\\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_screw_claw2}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step22}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Armgerüst aus \Cref{step20}
 \item[1x] M3x10mm-Rundkopfschraube
 \item[1x] M3-Schraubenmutter
\end{itemize}
Befestige die \textbf{M3x10mm-Rundkopfschraube} mithilfe der \textbf{M3-Schraubenmutter} an der markierten Stelle des Armgerüsts (\Cref{fig:mount_screw_claw2}).
\end{minipage}

<!-- Step 23 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_23.png}
\captionof{figure}{Montage der ersten Zangenhälfte an Armgerüst\\ Quelle: \ref{keyestudioManualArm}}\label{fig:mount_screw_claw2}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step23}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Armgerüst aus \Cref{step20}
 \item[1x] Zangenhälfte
 \item[1x] M3-Schraubenmutter
\end{itemize}
Stecke von unten die \textbf{Zangenhälfte} über die \textbf{M3x10mm-Rundkopfschraube aus \Cref{step22}} und befestige diese mithilfe der \textbf{M3-Schraubenmutter}. Das Armgerüst ist nun vollständig.
\end{minipage}

<!-- Step 24 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_24.png}
\captionof{figure}{Montage des Armgerüst an Basis\\ Quelle: \ref{keyestudioManual}}\label{fig:mount_arm_on_base}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step24}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Armgerüst aus \Cref{step20}
 \item[1x] Basis aus \Cref{step3}
 \item[1x] M2x5mm-Rundkopfschraube (liegt dem Servomotor bei)
\end{itemize}
Stecke das \textbf{Armgerüst aus \Cref{step20}} wie in \Cref{fig:mount_arm_on_base} auf die \textbf{Basis aus \Cref{step3}} und befestige beides zusätzlich mithilfe der \textbf{M2x5mm-Rundkopfschraube} an der markierten Stelle.
\end{minipage}

<!-- Step 25 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/robot_arm_installation_25.png}
\captionof{figure}{Montage Mikrocontrollers mit Kontrollboard an Basis mit Armgerüst\\ Quelle: \ref{keyestudioManual}}\label{fig:mount_mcu_on_base}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{step25}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] Basis mit Armgerüst aus \Cref{step24}
 \item[1x] Mikrocontroller
 \item[1x] Kontrollboard
 \item[8x] M3x6mm-Rundkopfschraube
 \item[4x] M3x10mm-Abstandshalter
\end{itemize}
Entferne zunächst das \textbf{Kontrollboard} vom \textbf{Mikrocontroller}. Befestige danach mithilfe der \textbf{M3x6mm-Rundkopfschrauben} und \textbf{M3x10mm-Abstandshalter} den \textbf{Mikrocontroller} an der \textbf{Basis mit Armgerüst aus \Cref{step24}}, wie in \Cref{fig:mount_mcu_on_base} zu sehen. Der Roboterarm ist nun soweit fertig, nun müssen nur noch die Servomotoren richtig angeschlossen werden.
\end{minipage}

\pagebreak

<!-- Step 26 -->
\begin{step}\label{step26}\end{step}
\textbf{Folgende Teile werden benötigt:}
\begin{itemize}
 \item[1x] Servomotor S0
 \item[1x] Servomotor S1
 \item[1x] Servomotor S2
 \item[1x] Servomotor S3
\end{itemize}
Schließe die **Servomotoren S0, S1, S2 und S3**, wie in \Cref{fig:connect_servos} zu sehen, an folgende Anschlüsse an:

| Servomotor | Anschluss |
| ---------- | --------- |
| S0         | A1        |
| S1         | A0        |
| S2         | 8         |
| S3         | 9         |
\captionof{table}{\label{servo_connection_table} Anschlüsse der einzelnen Servomotoren.}

![Anschluss der Servomotoren an Kontrollboard. Quelle: \ref{keyestudioManual} \label{fig:connect_servos}](./images/robot_arm_installation_26.png){width=75%}

### Der Zusammenbau des Controllers

Lucca bestaunt das Ergebnis ihrer Arbeit, es war zwar einiges an Bastelarbeit notwendig, aber jetzt wo sie den Arm vor sich hat, weiß sie, dass sich die Arbeit gelohnt hat. Voller Freude möchte sie gleich loslegen die ersten Teile damit zu transportieren, aber plötzlich fällt ihr etwas auf. Wie soll sie den Arm steuern? Aktuell könnte sie feste Programme schreiben, die eine bestimmte Bewegung durchführen, aber die müsste sie erst für jedes einzelne Teil herausfinden. Sie blickt nochmal zu den ganzen Teilen die sie mitgebracht hat und sieht, dass dort sich noch Teile eines Controllers befinden. Natürlich, den gab es ja auch noch, damit man den Arm manuell steuern kann. Also nimmt sie nochmal ihr Werkzeug in die Hand, um auch diesen zusammenzubauen.

:::note
Baue den Controller, wie in \Cref{fig:build_controller} zu sehen, zusammen.
:::

<!-- Controller Step 1 -->
\begin{minipage}[t]{0.45\textwidth}
\vspace{0pt}
\includegraphics[width=\textwidth]{./images/build_controller.png}
\captionof{figure}{Zusammenbau des Controllers\\ Quelle: \ref{keyestudioManual}}\label{fig:build_controller}
\end{minipage}
\hspace{0.05\textwidth}
\begin{minipage}[t]{0.45\textwidth}
\begin{step}\label{controller_step1}\end{step}
\textbf{Folgende Teile werden benötigt:}\begin{itemize}
 \item[1x] controllerförmiges Acrylteil
 \item[10x] M3x6mm-Rundkopfschraube
 \item[6x] M3x10mm-Abstandshalter
 \item[2x] Analogstick
\end{itemize}
Befestige die \textbf{M3x10mm-Abstandshalter} mithilfe der \textbf{M3x6mm-Rundkopfschrauben} am \textbf{controllerförmigen Acrylteil}. Lege danach die \textbf{Analogsticks} auf die \textbf{M3x10mm-Abstandshalter} und schraube diese mithilfe der restlichen \textbf{M3x6mm-Rundkopfschrauben} fest.
\end{minipage}

<!-- Controller Step 2 -->
\begin{step}\label{controller_step2}\end{step}
\textbf{Folgende Teile werden benötigt:}
\begin{itemize}
 \item[1x] Controller aus \Cref{controller_step1}
 \item[10x] Jumper-Kabel
\end{itemize}
Schließe die **Analogsticks des Controller aus \Cref{controller_step1}**, wie in \Cref{fig:connect_analog_sticks} zu sehen, an folgende Anschlüsse an:

| Analogstock links | Anschluss | Analogstick rechts | Anschluss |
| ----------------- | --------- | ------------------ | --------- |
| V                 | V         | V                  | V         |
| G                 | G         | G                  | G         |
| X                 | A3        | X                  | A2        |
| Y                 | A4        | Y                  | A5        |
| B                 | 6         | B                  | 7         |

\captionof{table}{\label{servo_connection_table} Anschlüsse der einzelnen Analogsticks.}

![Anschluss der Analogsticks an Kontrollboard. Quelle: \ref{keyestudioManual} \label{fig:connect_analog_sticks}](./images/controller_02_medium.png){width=75%}

\pagebreak

## Quellen

\begin{small}
\begin{enumerate}[label={[\arabic*]}]
\item \url{https://wiki.keyestudio.com/KS0488_Keyestudio_4DOF_Robot_Arm_DIY_Kit_V2.0_for_Arduino} \\
      (Letzter Zugriff: 21.02.2023 - 13:30 Uhr)\label{keyestudioManual}
\item \url{https://www.dropbox.com/sh/aepxqnvgjqtr8e4/AAAo_6kfndEaiD3tF3AsQSE0a/Install%20the%20claw%20of%20the%20robot%20arm.pdf?dl=0} \\
      (Letzter Zugriff: 25.02.2023 - 17:15 Uhr)\label{keyestudioManualArm}
\end{enumerate}
\end{small}
